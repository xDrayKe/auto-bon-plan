<?php
    session_start();
    if(!isset($_SESSION['role'])){
        header('Location: connexion.php');
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
   <link rel="shortcut icon" href="img/jra_icon.png" type="image/x-icon">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.scss">
    <link rel="stylesheet" type="text/css" href="css/title.css">
    <link rel="stylesheet" type="text/css" href="css/chart.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- CDN SCRIPT JS FOR BOOTSTRAP -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Autobonplan - TEST</title>
</head>
<body>
    <section class="main-header">
        <div class="mobile-menu">
        </div>
        <div class="header-infos">
            <div class="page-name">
                <div class="nav-toggler" title="Afficher le menu"><i class="fas fa-bars"></i></div> 
                <a href=""><img src="img/Logo_JRA_H_NOIR.png" alt=""></a>
                <h1>
                    Suivi hebdo
                </h1>
            </div>
            <div class="user">
                <img id="user_photo" src="" alt="">
                <div class="name">
                    <span>
                        <span id="firstname"></span> <span id="lastname"></span>
                    </span>
                    <i class="fas fa-chevron-down"></i>
                </div>
                <ul class="user-dropdown">
                    <li>
                        <a class="logout" href="deco.php">
                            <i class="fas fa-sign-out-alt"></i> Déconnexion
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="nav">
        <ul>
            <li data-name="accueil">
                <a href="acceuil.html" title="Liste des appels">
                    <svg width="40px" height="40px" class="stroke" viewBox="0 0 24.00 24.00" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20 17.0002V11.4522C20 10.9179 19.9995 10.6506 19.9346 10.4019C19.877 10.1816 19.7825 9.97307 19.6546 9.78464C19.5102 9.57201 19.3096 9.39569 18.9074 9.04383L14.1074 4.84383C13.3608 4.19054 12.9875 3.86406 12.5674 3.73982C12.1972 3.63035 11.8026 3.63035 11.4324 3.73982C11.0126 3.86397 10.6398 4.19014 9.89436 4.84244L5.09277 9.04383C4.69064 9.39569 4.49004 9.57201 4.3457 9.78464C4.21779 9.97307 4.12255 10.1816 4.06497 10.4019C4 10.6506 4 10.9179 4 11.4522V17.0002C4 17.932 4 18.3978 4.15224 18.7654C4.35523 19.2554 4.74432 19.6452 5.23438 19.8482C5.60192 20.0005 6.06786 20.0005 6.99974 20.0005C7.93163 20.0005 8.39808 20.0005 8.76562 19.8482C9.25568 19.6452 9.64467 19.2555 9.84766 18.7654C9.9999 18.3979 10 17.932 10 17.0001V16.0001C10 14.8955 10.8954 14.0001 12 14.0001C13.1046 14.0001 14 14.8955 14 16.0001V17.0001C14 17.932 14 18.3979 14.1522 18.7654C14.3552 19.2555 14.7443 19.6452 15.2344 19.8482C15.6019 20.0005 16.0679 20.0005 16.9997 20.0005C17.9316 20.0005 18.3981 20.0005 18.7656 19.8482C19.2557 19.6452 19.6447 19.2554 19.8477 18.7654C19.9999 18.3978 20 17.932 20 17.0002Z" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
                    <span>Accueil</span>
                </a>
            </li>
        <ul>
    </section>
    <section class="page-content">
    <div id="selectDiv">
        <?php
            //le formulaire avec les deux selects.
            include 'php/selectSem.php';
        ?>
    </div>

    <?php
        //On vas verifier si le visiteur a le droit d'upload un fichier
        if($_SESSION['role'] == "U"){    
            include_once 'html/upload.html'; 
        }
        if(isset($_POST['selectSem1']) && isset($_POST['selectSem2'])) {
            include 'php/M_data.php';
            $selectSem1 = $_POST['selectSem1'];
            $selectSem2 = $_POST['selectSem2'];

            //On recuperer tout les semaine allaint de lui selectionner vers le deuxieme selectionner tout en recuperer les autres entre eux : 18/20 -> 18-19-20
            $nombreWeek = $selectSem2-$selectSem1;

            //On recuperer les données de la BDD pour pouvoir les utiliser
            $data = getAllABPDataWithCondition($selectSem1, $selectSem2);

            $tableauAllCallSalepoint = array(); // Tableau de tous les call des salepoint
            $tableauAllCallPourcent= array(); //Tableau de tous les pourcent des salepoint

            $allSalepoint=  [];
            $totalAppel = 0;
            $totalDuration = 0;
            foreach ($data as $row) {
                $duration = $row->getDuration();
                if($duration != "00:00:00"){
                    $totalDuration++;
                }
                $salepoint = $row->getSalepoint();
                if (!in_array($salepoint, $allSalepoint)) {
                    $allSalepoint[] = $salepoint;
                }
                $tableauCallSalepoint = array(); // Tableau intermédiaire
                $tableauCallPourcent = array(); // Tableau intermédiaire
                for ($i = 0; $i <= $nombreWeek; $i++) { 
                    $week = $selectSem1 + $i;    //on parcourt tout les semaines 

                    $valeurCallFrommSale = getCallFromSalepointAndWeek($salepoint, $week, $week); //Ca nous donne le nombre d'appel avec un salepoint donnée et deux semaines.
                    $tableauCallSalepoint[$i] = $valeurCallFrommSale;

                    $valeurPourFromSale = getPourFromSalepointAndWeek($salepoint, $week, $week); //Ca nous donne le pourcentage(pour) d'un salepoint d'une semaine a une autres.
                    $tableauCallPourcent[$i] = $valeurPourFromSale;
                }
                $tableauAllCallSalepoint[] = $tableauCallSalepoint; // Ajouter le tableau interne au tableau de tableaux
                $tableauAllCallPourcent[] = $tableauCallPourcent; // Ajouter le tableau interne au tableau de tableaux
                $totalAppel++;
            } 
            //Fabriquation label
            $label= "['";
            foreach ($allSalepoint as $salepoint) {
                if(strpos($salepoint,"'")){
                    //d'Ol -> dOl => éviter tout probleme dans le label
                    $salepoint = str_replace("'", "", $salepoint);
                }
            $label .= $salepoint."','";  
            }
            $label = substr($label, 0, -3); //permet d'enlever les 3 derniers caracteres pour permttre une bonne formalisme
            $label .= "']";


            //Elles servent pour le premier graphisque (en hauteur)
            $data0 = "["; $data1 = "["; $data2 = "["; $data3 = "["; //Ces variables servent pour le chart.js et mettre les données dans un bon format acceptable pour Chart.js

            //Elles servent pour le premier graphisque (en longueur)
            $data20 = "["; $data21 = "["; $data22 = "["; $data23 = "["; //Ces variables servent pour le chart.js et mettre les données dans un bon format acceptable pour Chart.js
            $longueur = count($tableauAllCallSalepoint)-1;
            for ($i = 0; $i <= $longueur; $i++) { 
                for ($j = 0; $j <= $nombreWeek; $j++) { 
                    if($i == $longueur){ ${'data'.$j} .= $tableauAllCallSalepoint[$i][$j]."]"; ${'data2'.$j} .= intval($tableauAllCallPourcent[$i][$j])."]";} //Si on est a la fin on ferme le crochet sinon on continuent a ajouter les données a la suite dans un formalisme défini
                    else{ ${'data'.$j} .= $tableauAllCallSalepoint[$i][$j].", "; ${'data2'.$j} .= intval($tableauAllCallPourcent[$i][$j]).", "; } 
                }
            }
            //Titre total Appel
            $pourAppel = round(($totalDuration/$totalAppel)*100,0);
            echo "<div class='divTitle'>";
            echo "<h1 class='title'>Total pris en charge : ".$pourAppel."%</h1>";
            echo "<h1 class='title2'>Total appels reçus : ".$totalAppel."</h1>";
            echo "</div>";
      ?>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<div class="chart-container">
    <canvas id="myChartCanvas"></canvas>
    <canvas id="myChartCanvas2"></canvas>
</div>

<script>
    var ctx = document.getElementById('myChartCanvas').getContext('2d');
    var label = <?php echo $label ?>;
    var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: label,
        datasets: [
            {
                label: 'Semaine <?php echo $selectSem1 ?>',
                data: <?php echo $data0 ?>,
                backgroundColor: 'rgba(192,98,43,0.20)',
                borderColor: 'rgba(192,98,43,1)',
                borderWidth: 1
            },
            <?php
                if($nombreWeek >= 1){
                    $semaine = $selectSem1+1;
                    echo  "{
                        label: 'Série $semaine',
                        data: $data1,
                        backgroundColor: 'rgba(244,59,255,0.2)',
                        borderColor: 'rgba(244,59,255,1)',
                        borderWidth: 1
                    },";
                }
                if($nombreWeek >= 2){
                    $semaine = $selectSem1+2;
                    echo  "{
                        label: 'Série $semaine',
                        data: $data2,
                        backgroundColor: 'rgba(45,193,237,0.2)',
                        borderColor: 'rgba(45,193,237,1)',
                        borderWidth: 1
                    },";
                }
                if($nombreWeek == 3){
                    $semaine = $selectSem1+3;
                    echo  "{
                        label: 'Série $semaine',
                        data: $data3,
                        backgroundColor: 'rgba(200,141,49,0.2)',
                        borderColor: 'rgba(200,141,49,1)',
                        borderWidth: 1
                    },";
                }
            ?>
        ]
    },
    options: {
        aspectRatio: 1, 
        scales: {
                    y: {
                        beginAtZero: true
                    }
                }
    }
});
</script>

<script>
    var ctx = document.getElementById('myChartCanvas2').getContext('2d');
    var label = <?php echo $label ?>;
        var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: label,
        datasets: [
            {
                label: 'Semaine <?php echo $selectSem1 ?>',
                data: <?php echo $data20 ?>,
                backgroundColor: 'rgba(192,98,43,0.20)',
                borderColor: 'rgba(192,98,43,1)',
                borderWidth: 1
            },
            <?php
                if($nombreWeek >= 1){
                    $semaine = $selectSem1+1;
                    echo  "{
                        label: 'Semaine $semaine',
                        data: $data21,
                        backgroundColor: 'rgba(244,59,255,0.2)',
                        borderColor: 'rgba(244,59,255,1)',
                        borderWidth: 1
                    },";
                }
                if($nombreWeek >= 2){
                    $semaine = $selectSem1+2;
                    echo  "{
                        label: 'Semaine $semaine',
                        data: $data22,
                        backgroundColor: 'rgba(45,193,237,0.2)',
                        borderColor: 'rgba(45,193,237,1)',
                        borderWidth: 1
                    },";
                }
                if($nombreWeek == 3){
                    $semaine = $selectSem1+3;
                    echo  "{
                        label: 'Semaine $semaine',
                        data: $data23,
                        backgroundColor: 'rgba(200,141,49,0.2)',
                        borderColor: 'rgba(200,141,49,1)',
                        borderWidth: 1
                    },";
                }
            ?>
        ]
    },
    options:{
        aspectRatio: 1, 
        indexAxis: 'y',
  plugins: {
    datalabels: {
      anchor: 'end',
      align: 'end',
      formatter: function(value) {
        return value + '%';
      },
      display: true,
      color: 'black'
    }
  },
  scales: {
    x: {
      beginAtZero: true,
      ticks: {
        callback: function(value) {
          return value + '%';
        }
      }
    }
  }
}
});
</script>

<?php } ?>
</section>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/upload.js"></script>
</body>
</html>