<?php
  session_start();
   if(isset($_POST['login']) && isset($_POST['password'])) {
       include "../id.php";
       $loginPost = $_POST['login'];
       $passwordPost = $_POST['password'];

       try {

           $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
           $sql = "SELECT * FROM `ABP_gestion` WHERE login = :login";
           $stmt = $conn->prepare($sql);
           $stmt->bindParam(':login', $loginPost);
           $stmt->execute();
           $row = $stmt->fetch(PDO::FETCH_ASSOC);

           if ($row && password_verify($passwordPost, $row['password'])) {

               $_SESSION['username'] = $row['login'];
               header('Location: index.php');
               
           } else {
               echo "Identifiant ou mot de passe incorrect";
           }
       } catch(PDOException $e) {
           echo "Erreur de connexion à la base de données : " . $e->getMessage();
       }
   } 
?>
<link rel="stylesheet" href="../css/connexion.css">
<title>Connexion</title>
<body>
  <div class="container">
    <h2>Connexion</h2>
    <form method="POST">
      <label for="login">Nom d'utilisateur :</label>
      <input type="text" id="login" name="login" required>
      <label for="password">Mot de passe :</label>
      <input type="password" id="password" name="password" required>
      <input type="submit" value="Se connecter">
    </form>
  </div>