<?php
    include 'id.php';
    echo '<link rel="stylesheet" type="text/css" href="css/form.css">';
    echo '<form class="formSelect" method="POST" action="">';
    echo '<label for="selectSem1">Selectionner la première semaine : </label>';
    try {
        $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Exécution de la requête SQL pour récupérer tous les enregistrements
        $stmt = $conn->prepare('SELECT WEEK(dataDate) AS weekNumber
        FROM ABP_data
        GROUP BY weekNumber');
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        echo "<select id='selectSem1' name='selectSem1'>";
        echo "<option value=' '> Choisir une semaine</option>";
        foreach ($results as $row) {
            echo "<option value='".$row['weekNumber']."'>Semaine ".$row['weekNumber']."</option>";
        }
        echo "</select>";
    } catch (PDOException $e) {
        // Gestion des erreurs lors de la connexion à la base de données
        echo 'Erreur : ' . $e->getMessage();
    }
    echo "<br>";
    echo '<label for="selectSem1">Selectionner la deuxieme semaine : </label>';
    try {
        $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Exécution de la requête SQL pour récupérer tous les enregistrements
        $stmt = $conn->prepare('SELECT WEEK(dataDate) AS weekNumber
        FROM ABP_data
        GROUP BY weekNumber');
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo "<select id='selectSem2' name='selectSem2'>";
        echo "<option value=' '> Choisir une semaine</option>";
        foreach ($results as $row) {
            echo "<option value='".$row['weekNumber']."'>Semaine ".$row['weekNumber']."</option>";
        }
        echo "</select>";
    } catch (PDOException $e) {
        // Gestion des erreurs lors de la connexion à la base de données
        echo 'Erreur : ' . $e->getMessage();
    }
    echo "<br>";
    echo "<input value='Envoyer' type='submit'>";
    echo "</form>";
?>