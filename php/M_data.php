<?php

class ABPData {
    private $idData;
    private $call_3cx_id;
    private $duration;
    private $dataDate;
    private $salepoint;
    private $salepoint_phone;

    // Constructeur
    public function __construct($idData,$call_3cx_id, $duration, $dataDate, $salepoint, $salepoint_phone) {
        $this->idData = $idData;
        $this->call_3cx_id = $call_3cx_id;
        $this->duration = $duration;
        $this->dataDate = $dataDate;
        $this->salepoint = $salepoint;
        $this->salepoint_phone = $salepoint_phone;
    }

    // Getter pour idData
    public function getIdData() {
        return $this->idData;
    }
    public function setIdData($call_3cx_id) {
        $this->idData = $idData;
    }

    // Getter et Setter pour call_3cx_id
    public function getCall3cxId() {
        return $this->call_3cx_id;
    }

    public function setCall3cxId($call_3cx_id) {
        $this->call_3cx_id = $call_3cx_id;
    }

    // Getter et Setter pour duration
    public function getDuration() {
        return $this->duration;
    }

    public function setDuration($duration) {
        $this->duration = $duration;
    }

    // Getter et Setter pour dataDate
    public function getDataDate() {
        return $this->dataDate;
    }

    public function setDataDate($dataDate) {
        $this->dataDate = $dataDate;
    }

    // Getter et Setter pour salepoint
    public function getSalepoint() {
        return $this->salepoint;
    }

    public function setSalepoint($salepoint) {
        $this->salepoint = $salepoint;
    }

    // Getter et Setter pour salepoint_phone
    public function getSalepointPhone() {
        return $this->salepoint_phone;
    }

    public function setSalepointPhone($salepoint_phone) {
        $this->salepoint_phone = $salepoint_phone;
    }
}

// Méthode pour récupérer tous les enregistrements dans une liste d'objets de la classe ABPData
function getAllABPData() {
    // Code pour se connecter à la base de données

    include 'id.php';

    try {
        $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Exécution de la requête SQL pour récupérer tous les enregistrements
        $stmt = $conn->query('SELECT * FROM ABP_data');
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $abpDataList = [];
        foreach ($results as $row) {
            $abpData = new ABPData($row['idData'],$row['call_3cx_id'], $row['duration'], $row['dataDate'], $row['salepoint'], $row['salepoint_phone']);
            $abpDataList[] = $abpData;
        }

        return $abpDataList;
    } catch (PDOException $e) {
        // Gestion des erreurs lors de la connexion à la base de données
        echo 'Erreur : ' . $e->getMessage();
    }
}
function getAllABPDataWithCondition($weekNumber1, $weekNumber2) {
    include 'id.php';
    try {
        $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $conn->query('SELECT * FROM ABP_data WHERE EXTRACT(WEEK FROM dataDate) BETWEEN '.$weekNumber1.' AND '.$weekNumber2); //.'ORDER BY salepoint';
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $abpDataList = [];
        foreach ($results as $row) {
            $abpData = new ABPData($row['idData'],$row['call_3cx_id'], $row['duration'], $row['dataDate'], $row['salepoint'], $row['salepoint_phone']);
            $abpDataList[] = $abpData;
        }

        return $abpDataList;
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }
}


function getAllABPsalepoint(){
    include 'id.php';

    try {
        $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $conn->query('SELECT salepoint from ABP_data group by salepoint');
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $list = [];
        foreach ($results as $row) {
            $list[] = $row['salepoint'];
        }

        return $list;
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }
}

function getCallFromSalepointAndWeek($salepoint,$weekNumber1,$weekNumber2){
    include 'id.php';

    try {
        $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $conn->prepare('SELECT COUNT(*) AS call_count
        FROM ABP_data
        WHERE salepoint = :salepoint
        AND EXTRACT(WEEK FROM dataDate) BETWEEN :date1 AND :date2
        GROUP BY WEEK(dataDate)');
        $stmt->bindParam(':salepoint', $salepoint, PDO::PARAM_STR);
        $stmt->bindParam(':date1', $weekNumber1, PDO::PARAM_STR);
        $stmt->bindParam(':date2', $weekNumber2, PDO::PARAM_STR);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($results as $row) {
            $return = $row['call_count'];
        }
        if(empty($return )){
            $return = 0; // Définir la valeur sur 0 si non définie
        }
        return $return;
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }
}

function getPourFromSalepointAndWeek($salepoint,$weekNumber1,$weekNumber2){
    include 'id.php';

    try {
        $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $stmt = $conn->prepare("SELECT
        (SUM(CASE WHEN duration > '00:00:00' THEN 1 ELSE 0 END) / COUNT(duration)) * 100 AS pourcentAppel 
        FROM ABP_data WHERE salepoint = :salepoint AND EXTRACT(WEEK FROM dataDate) BETWEEN :date1 AND :date2;");
        $stmt->bindParam(':salepoint', $salepoint, PDO::PARAM_STR);
        $stmt->bindParam(':date1', $weekNumber1, PDO::PARAM_STR);
        $stmt->bindParam(':date2', $weekNumber2, PDO::PARAM_STR);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($results as $row) {
            $return = $row['pourcentAppel'];
        }
        if(empty($return )){
            $return = 100; // Définir la valeur sur 0 si non définie
        }
        return $return;
    } catch (PDOException $e) {
        echo 'Erreur : ' . $e->getMessage();
    }
}


?>
