<?php
include "id.php";
if(isset($_FILES['fichier']) && $_FILES['fichier']['error'] === UPLOAD_ERR_OK){
        $nomFichier = $_FILES['fichier']['tmp_name'];
    if (($fichier = fopen($nomFichier, "r")) !== false) {
        $donnees = fgetcsv($fichier, 1000, ",");
        $sql = "INSERT INTO ABP_data (call_3cx_id, duration, dataDate, salepoint, salepoint_phone) VALUES ";
        while (($donnees = fgetcsv($fichier, 1000, ",")) !== false) {
            $data = explode(";",$donnees[0]);
            //Pour d'Olone
            if(strpos($data[3], "'")){
                $data[3] = str_replace("'", "''", $data[3]);
            }
            $datetime = DateTime::createFromFormat('d/m/Y H:i', $data[2]);
            $data[2] = $datetime->format('Y-m-d H:i:s');

            //$time = strtotime($data[2]);
            //$data[2] = date("Y-m-d H:i:s", $time);
            $sql.= "('".$data[0]."','".$data[1]."','".$data[2]."','".$data[3]."',".$data[4]."), ";
            header("Location: accueil.php");
        }
        fclose($fichier);
        $sql = substr($sql, 0, -2);
        $conn = new PDO("mysql:host=$host;dbname=$dataBase", $login, $password);
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    } else {
        echo "Impossible d'ouvrir le fichier CSV.";
    }
}

?>