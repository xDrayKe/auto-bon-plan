
$( document ).ready(function() {
    $('.nav-toggler').click(function(){
        $('.nav ul:first-child').toggleClass('open')
        $('.page-content').toggleClass("min-nav")
    })
    
    $('.name').click(function(){
        $('.user-dropdown').toggle()
        $('.name i').toggleClass("fa-chevron-down")
        $('.name i').toggleClass("fa-chevron-up")
    })
})