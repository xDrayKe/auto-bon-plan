const fileInput = document.getElementById('fileInput');
const uploadForm = document.getElementById('uploadForm');

fileInput.addEventListener('change', () => {
    uploadForm.submit();
});